#include "TV785Histogram.h"

#include "TV792Data.hxx"
#include "TDirectory.h"

const int Nchannels = 32;
const int Nmodules = 5;
#include <sys/time.h>

/// Reset the histograms for this canvas
TV785Histograms::TV785Histograms(){  
  
  CreateHistograms();
}


void TV785Histograms::CreateHistograms(){
  

  // Otherwise make histograms
  clear();
  
  for( int j = 0; j < Nmodules;j++){
	  for(int i = 0; i < Nchannels; i++){ // loop over channels    

		  char name[100];
		  char title[100];
		  sprintf(name,"V785_%i_%i",j,i);

		  // Delete old histograms, if we already have them
		  TH1D *old = (TH1D*)gDirectory->Get(name);
		  if (old){
			  delete old;
		  }


		  // Create new histograms

		  sprintf(title,"V785 Module %i,  channel=%i",j,i);	

		  TH1D *tmp = new TH1D(name,title,4200,0,4200);
		  tmp->SetYTitle("ADC value");
		  tmp->SetXTitle("Bin");
		  push_back(tmp);
	  }//loop2
  }//loop1

}



  
/// Update the histograms for this canvas.
void TV785Histograms::UpdateHistograms(TDataContainer& dataContainer){

  struct timeval start,stop;
 gettimeofday(&start,NULL);
 //printf ("About to start request: %f\n",start.tv_sec
  //  + 0.000001*start.tv_usec); 

  TV792Data *data = dataContainer.GetEventData<TV792Data>("ADC0");
  if(!data) return;

  //data->Print();
  /// Get the Vector of ADC Measurements.
  std::vector<VADCMeasurement> measurements = data->GetMeasurements();
  for(unsigned int i = 0; i < measurements.size(); i++){ // loop over measurements
	
    int chan = measurements[i].GetChannel();
    uint32_t adc = measurements[i].GetMeasurement();
    uint32_t modl = measurements[i].GetModule();

    if(chan >= 0 && chan < Nchannels){
      int pos = Nchannels*modl + chan;
      GetHistogram(pos)->Fill(adc);

    }


  }

}



/// Take actions at begin run
void TV785Histograms::BeginRun(int transition,int run,int time){

  CreateHistograms();

}

/// Take actions at end run  
void TV785Histograms::EndRun(int transition,int run,int time){

}
