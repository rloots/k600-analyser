/*
 * =====================================================================================
 *
 *       Filename:  TEpics.hxx
 *
 *    Description: Class for the Samples from SIS3302 ADC Measurement. 
 *
 *        Version:  1.0
 *        Created:  19/07/2015
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), lcpool@tlabs.ac.za
 *        Company:  Ithemba Labs.
 *
 * =====================================================================================
 */

#ifndef TEpics_hxx_seen
#define TEpics_hxx_seen

#include <vector>
#include "TGenericData.hxx"

/// Class for each epics measurement
class VEPICSMeasurement {

  friend class TEpicsData;

public:
  
  /// Get a Raw Sample
  float GetRawMeasurement() const {return (epics_measurement_raw_word);}

 private:

  /// Fields to hold the header, measurement, trailer and error words.
  float epics_measurement_raw_word;

  /// Constructor; need to pass in header and measurement.
  VEPICSMeasurement(float raw_sample):
    epics_measurement_raw_word(raw_sample){
  }


  VEPICSMeasurement();    
};


/// Class for storing data for Epics variables
class TEpicsData: public TGenericData {

 
public:

  /// Constructor
  TEpicsData(int bklen, int bktype, const char* name, void *pdata);

  void Print();
  
  /// Get the Vector of Epics Measurements.
  std::vector<VEPICSMeasurement>& GetMeasurements() {return fMeasurements;}


private:

  std::vector<VEPICSMeasurement> fMeasurements;

};


#endif
