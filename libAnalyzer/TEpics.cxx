/*
 * =====================================================================================
 *
 *       Filename:  TEpics.cxx
 *
 *    Description: Class for the Samples for Epics Measurement. 
 *
 *        Version:  1.0
 *        Created:  19/07/2015
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), lcpool@tlabs.ac.za
 *        Company:  Ithemba Labs.
 *
 * =====================================================================================
 */

#include "TEpics.hxx"

/// Constructor
TEpicsData::TEpicsData(int bklen, int bktype, const char* name, void *pdata):
  TGenericData(bklen, bktype, name, pdata)
{
	std::cout << " Epics Datum: size " << GetSize() << std::endl;

  float *vdata =(float*)pdata;
  for(int i = 0; i < GetSize(); i++){
    float word = (float)(*vdata);
    *vdata++;
    std::cout << " epics value: "<<word<<std::endl;
    fMeasurements.push_back(VEPICSMeasurement(word));
  }//For    

}//Constructor

void TEpicsData::Print(){
	std::cout << "Epics decoder for bank " << GetName().c_str() << std::endl;
	for(int i = 0; i < fMeasurements.size(); i++){
		std::cout << " meas = " << fMeasurements[i].GetRawMeasurement() << std::endl;
		//if((i-2)%3 == 0) std::cout << std::endl;
	}
	std::cout << std::endl;
}//Printf



